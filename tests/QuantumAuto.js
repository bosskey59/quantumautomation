import { Selector} from 'testcafe';


const codeInject1 = `
(function() {
  var qtm = document.createElement('script'); qtm.type = 'text/javascript'; qtm.async = 1;
  qtm.src = 'https://cdn.quantummetric.com/instrumentation/quantum-qa1.js';
  var d = document.getElementsByTagName('script')[0];
  !window.QuantumMetricAPI && d.parentNode.insertBefore(qtm, d);
  })();
  window["QuantumMetricOnload"] = function() {
  var sessionID = QuantumMetricAPI.getSessionID();
  console.log(sessionID)
  // <VERIFY HERE THAT sessionID IS UNDEFINED>
  };
`

const sampleSession = 'f177810b61d9d51cc8f86a5d11030e33'

const codeInject2 = `
(function() {
    var qtm = document.createElement('script'); qtm.type = 'text/javascript'; qtm.async = 1;
    qtm.src = 'https://cdn.quantummetric.com/instrumentation/quantum-qa1.js';
    var d = document.getElementsByTagName('script')[0];
    !window.QuantumMetricAPI && d.parentNode.insertBefore(qtm, d);
    })();
    window["QuantumMetricOnload"] = function() {
    QuantumMetricAPI.addEventListener('start', function() {
    var sessionID = QuantumMetricAPI.getSessionID();
    console.log(sessionID)
    // <VERIFY HERE THAT sessionID IS DEFINED>
    });
    };
`

fixture `Quantum Metric Automation Challenge`
    .page `https://www.quantummetric.com/our-story/`;

const marioImage = Selector('#person-5').withAttribute('xlink:href','https://www.quantummetric.com/assets/uploads/5.png');

test('Presence of Mario’s image,', async t => {
    await t
        .expect(marioImage.exists).ok();
});



fixture `Quantum Metric Automation Challenge`
    .page `https://status.cloud.google.com/`;
    
// making assumption that nothing else will be console logged
// could also set a global var and check that as an alt option
test('Session ID undefined', async t => {
    const { log } = await t.getBrowserConsoleMessages()
    await t.expect(log).contains("undefined")
})
.clientScripts({ content: codeInject1 });

// making assumption that only our sessionID will be 32 lowecaseAlphanumeric long (25/25 it was)
test('Session ID is present', async t => {
    const { log } = await t.getBrowserConsoleMessages()
    const regexTest = /^(?=.{32}$)[a-z0-9]*$/
    const passedRegex = log.some(loggedMessage => regexTest.test(loggedMessage))
    await t.expect(passedRegex).ok()
})
.clientScripts({ content: codeInject2 });
